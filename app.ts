import express from 'express';
import path from 'path';
import hbs from 'hbs';
import bodyParser from 'body-parser';
import flash from 'express-flash';
const app = express();
const publicRoutes = require('./routes/public.js');
const adminRoutes = require('./routes/admin.js');
const authRoutes = require('./routes/auth.js');
const session = require('express-session');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

hbs.registerPartials(path.join(__dirname, 'views', 'partials'));

app.use(session({
    secret: 'Inge-3',
    resave: false,
    saveUninitialized: false,
    cookie: { secure: false }
}));
app.use(flash());

app.use("/admin", adminRoutes);
app.use('/', publicRoutes);
app.use('/auth', authRoutes);

app.listen(3000, () => {
    console.log("Server is running on port http://localhost:3000/");
});
