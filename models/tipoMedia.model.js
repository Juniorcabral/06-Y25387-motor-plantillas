const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const dbPath = path.resolve(__dirname, '../db/Inge3.db');
const db = new sqlite3.Database(dbPath);

function getTipoMedia(filters = {}) {
    return new Promise((resolve, reject) => {
        let sql = 'SELECT * FROM tipo_media WHERE 1=1';
        const params = [];

        if (filters.nombre) {
            sql += ' AND nombre LIKE ?';
            params.push(`%${filters.nombre}%`);
        }
        if (filters.activo !== undefined && filters.activo !== '') {
            sql += ' AND activo = ?';
            params.push(filters.activo);
        }

        sql += ' ORDER BY orden ASC';

        db.all(sql, params, (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}

function createTipoMedia(tipoMedia) {
    return new Promise((resolve, reject) => {
        db.run('INSERT INTO tipo_media (nombre, activo, orden) VALUES (?, ?, ?)', [tipoMedia.nombre, 1, tipoMedia.orden], (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function editTipoMedia(tipoMedia, tipoMediaID) {
    return new Promise((resolve, reject) => {
        db.run('UPDATE tipo_media SET nombre = ? WHERE id = ?', [tipoMedia.nombre, tipoMediaID], (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function deleteTipoMedia(tipoMediaID) {
    return new Promise((resolve, reject) => {
        db.all('SELECT * FROM media WHERE tipo_media_id = ?', [tipoMediaID], (err, rows) => {
            if (err) {
                reject(err);
            } else if (rows.length > 0) {
                reject('El tipo media tiene medias asociados, no se puede eliminar.');
            } else {
                db.run('UPDATE tipo_media SET activo = 0 WHERE id = ?', [tipoMediaID], (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            }
        });
    });
}

function getTipoMediaByID(tipoMediaID) {
    return new Promise((resolve, reject) => {
        db.get('SELECT * FROM tipo_media WHERE id = ?', [tipoMediaID], (err, row) => {
            if (err) {
                reject(err);
            } else {
                resolve(row);
            }
        });
    });
}

module.exports = {
    getTipoMedia,
    createTipoMedia,
    editTipoMedia,
    deleteTipoMedia,
    getTipoMediaByID
};
