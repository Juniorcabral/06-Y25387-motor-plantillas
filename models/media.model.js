const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const dbPath = path.resolve(__dirname, '../db/Inge3.db');
const db = new sqlite3.Database(dbPath);

function getMedia(filters = {}) {
    return new Promise((resolve, reject) => {
        let sql = 'SELECT m.*, i.nombre as nombreIntegrante, tm.nombre as nombreTipoMedia FROM media m JOIN integrantes i ON m.matricula = i.matricula JOIN tipo_media tm ON m.tipo_media_id = tm.id WHERE 1=1';
        const params = [];

        if (filters.integrante) {
            sql += ' AND i.matricula = ?';
            params.push(filters.integrante);
        }

        if (filters.tipo_media) {
            sql += ' AND tm.id = ?';
            params.push(filters.tipo_media);
        }
        if (filters.activo !== undefined && filters.activo !== '') {
            sql += ' AND m.activo = ?';
            params.push(filters.activo);
        }
        if (filters.matricula) {
            sql += ' AND i.matricula LIKE ?';
            params.push(`%${filters.matricula}%`);
        }

        sql += ' ORDER BY m.orden ASC';

        db.all(sql, params, (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}

function createMedia(media, destino) {
    return new Promise((resolve, reject) => {
        db.run('INSERT INTO media (matricula, tipo_media_id, titulo, alt, orden, url, src, activo) VALUES (?,?,?,?,?,?,?,?)', [media.integrante, media.tipo_media, media.titulo, media.descripcion, media.orden, media.url, destino, 1], (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function updateMedia(mediaID, media) {
    return new Promise((resolve, reject) => {
        db.run('UPDATE media SET titulo = ?, alt = ? WHERE id = ?', [media.titulo, media.descripcion, mediaID], (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function deleteMedia(mediaID) {
    return new Promise((resolve, reject) => {
        db.run('UPDATE media SET activo = 0 WHERE id = ?', [mediaID], (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function getMediaID(mediaID) {
    return new Promise((resolve, reject) => {
        db.get('SELECT m.*, i.nombre as nombreIntegrante, tm.nombre as nombreTipoMedia FROM media m JOIN integrantes i ON m.matricula = i.matricula JOIN tipo_media tm ON m.tipo_media_id = tm.id WHERE m.id = ?', [mediaID], (err, row) => {
            if (err) {
                reject(err);
            } else {
                resolve(row);
            }
        });
    });
}

module.exports = {
    getMedia,
    createMedia,
    updateMedia,
    deleteMedia,
    getMediaID
};
