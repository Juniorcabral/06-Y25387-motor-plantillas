const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const dbPath = path.resolve(__dirname, '../db/Inge3.db');
const db = new sqlite3.Database(dbPath);

function getAllIntegrantes(filters = {}) {
    return new Promise((resolve, reject) => {
        let sql = 'SELECT * FROM integrantes WHERE 1=1';
        const params = [];

        if (filters.nombre) {
            sql += ' AND nombre LIKE ?';
            params.push(`%${filters.nombre}%`);
        }
        if (filters.apellido) {
            sql += ' AND apellido LIKE ?';
            params.push(`%${filters.apellido}%`);
        }
        if (filters.matricula) {
            sql += ' AND matricula LIKE ?';
            params.push(`%${filters.matricula}%`);
        }

        sql += ' ORDER BY orden ASC';

        db.all(sql, params, (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}

function getIntegranteByMatricula(matricula) {
    return new Promise((resolve, reject) => {
        db.get('SELECT * FROM integrantes WHERE activo = 1 AND matricula = ?', [matricula], (err, row) => {
            if (err) {
                reject(err);
            } else {
                resolve(row);
            }
        });
    });
}

function createIntegrante(integrante) {
    return new Promise((resolve, reject) => {
        db.run('INSERT INTO integrantes (nombre, apellido, matricula, descripcion, orden, activo) VALUES (?, ?, ?, ?, ?, ?)', [integrante.nombre, integrante.apellido, integrante.matricula, integrante.descripcion, integrante.orden, 1], (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function editIntegrante(matricula, integrante) {
    return new Promise((resolve, reject) => {
        db.run('UPDATE integrantes SET nombre = ?, apellido = ?, descripcion = ? WHERE matricula = ?', [integrante.nombre, integrante.apellido, integrante.descripcion, matricula], (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function deleteIntegrante(matricula) {
    return new Promise((resolve, reject) => {
        db.all('SELECT * FROM media WHERE matricula = ?', [matricula], (err, rows) => {
            if (err) {
                reject(err);
            } else if (rows.length > 0) {
                reject('El integrante tiene medios asociados, no se puede eliminar.');
            } else {
                db.run('UPDATE integrantes SET activo = 0 WHERE matricula = ?', [matricula], (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            }
        });
    });
}

module.exports = {
    getAllIntegrantes,
    getIntegranteByMatricula,
    createIntegrante,
    editIntegrante,
    deleteIntegrante
};
