const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const bcrypt = require('bcrypt');
const dbPath = path.resolve(__dirname, '../db/Inge3.db');
const db = new sqlite3.Database(dbPath);

function getUserByEmail(email) {
    return new Promise((resolve, reject) => {
        db.get('SELECT * FROM usuarios WHERE email = ?', [email], (err, row) => {
            if (err) {
                reject(err);
            } else {
                resolve(row);
            }
        });
    });
}

function createUser(email, password, superUser, matricula) {
    return new Promise(async (resolve, reject) => {
        try {
            const saltRounds = 10;
            const hashedPassword = await bcrypt.hash(password, saltRounds);
            db.run('INSERT INTO usuarios (email, contrasenha, super_user, matricula) VALUES (?, ?, ?, ?)', [email, hashedPassword, superUser, matricula], (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        } catch (err) {
            reject(err);
        }
    });
}

module.exports = {
    getUserByEmail,
    createUser
};
