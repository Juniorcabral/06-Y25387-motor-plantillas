const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const dbPath = path.resolve(__dirname, '../db/Inge3.db');
const db = new sqlite3.Database(dbPath);

function getHome() {
    return new Promise((resolve, reject) => {
        db.all('SELECT * FROM home', [], (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}

function getInformacion() {
    return new Promise((resolve, reject) => {
        db.all('SELECT * FROM informacion', [], (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}

module.exports = {
    getHome,
    getInformacion
};
