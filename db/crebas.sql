CREATE TABLE IF NOT EXISTS "integrantes" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "matricula" TEXT NOT NULL,
    "nombre" TEXT NOT NULL,
    "apellido" TEXT NOT NULL,
    "descripcion" TEXT,
    "activo" INTEGER DEFAULT 1,
    "orden" INTEGER
);

CREATE TABLE IF NOT EXISTS "tipo_media" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "nombre" TEXT NOT NULL,
    "activo" INTEGER DEFAULT 1,
    "orden" INTEGER
);

CREATE TABLE IF NOT EXISTS "media" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "src" TEXT,
    "url" TEXT,
    "titulo" TEXT,
    "alt" TEXT,
    "activo" INTEGER DEFAULT 1,
    "orden" INTEGER,
    "matricula" TEXT NOT NULL,
    "tipo_media_id" INTEGER NOT NULL,
    FOREIGN KEY ("tipo_media_id") REFERENCES "tipo_media"("id")
    ON UPDATE NO ACTION ON DELETE NO ACTION,
    FOREIGN KEY ("matricula") REFERENCES "integrantes"("matricula")
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE TABLE IF NOT EXISTS "Home" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "nombre" TEXT,
    "titulo" TEXT,
    "src" TEXT,
    "alt" TEXT,
    "descripcion" TEXT
);

CREATE TABLE IF NOT EXISTS "informacion" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "href" TEXT,
    "nombre" TEXT
);

CREATE TABLE IF NOT EXISTS "usuarios" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "email" TEXT NOT NULL,
    "contrasenha" TEXT NOT NULL,
    "super_user" INTEGER NOT NULL,
    "matricula" TEXT,
    FOREIGN KEY ("matricula") REFERENCES "integrantes"("matricula")
    ON UPDATE NO ACTION ON DELETE NO ACTION
);