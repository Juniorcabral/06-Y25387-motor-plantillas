-- Integrantes
INSERT INTO integrantes (matricula, nombre, apellido, descripcion, activo, orden) VALUES
('Y25387', 'Junior', 'Cabral', 'Programador backend, físico y astrónomo por hobby', 1, 1),
('Y19937', 'Juan', 'Aquino', 'Estudiante de la carrera de Análisis de Sistemas Informáticos en la Universidad Católica Nuestra Señora de la Asunción sede Guarambaré', 1, 2),
('Y25495', 'Sebastian', 'Pereira', 'Estudiante de la carrera de Análisis de Sistemas Informáticos en la Universidad Católica Nuestra Señora de la Asunción sede Guarambaré', 1, 3),
('Y19099', 'Elvio', 'Aguero', 'Estudiante de la carrera de Análisis de Sistemas Informáticos en la Universidad Católica Nuestra Señora de la Asunción sede Guarambaré', 1, 4),
('UG0085', 'Luis', 'Delgado', 'Estudiante de la carrera de Análisis de Sistemas Informáticos en la Universidad Católica Nuestra Señora de la Asunción sede Guarambaré', 1, 5);

-- Tipo de media
INSERT INTO tipo_media (nombre, activo, orden) VALUES
('Youtube', 1, 1),
('Dibujo', 1, 2),
('Imagen', 1, 3);

-- Media
INSERT INTO media (src, url, titulo, alt, activo, orden, matricula, tipo_media_id) VALUES
(NULL, 'https://www.youtube.com/embed/Tsnyq-3k7Bg?si=-Bzir8axv4WRU4MS&amp;controls=0', 'Mi Video Favorito en YouTube', 'Video de Quantum Fracture', 1, 1, 'Y25387', 1),
('/assets/Junior-Dibujo.png', NULL, 'Dibujo favorito', 'Dibujo de un planeta con anillos', 1, 2, 'Y25387', 2),
('/assets/Junior-Foto.jpg', NULL, 'Imagen favorita', 'Atardecer desde Ypané', 1, 3, 'Y25387', 3),

(NULL, 'https://www.youtube.com/embed/_Yhyp-_hX2s', 'Mi Video Favorito en YouTube', 'Video de Eminem', 1, 4, 'Y19937', 1),
('/assets/Juan-Dibujo.jpg', NULL, 'Dibujo favorito', 'Dibujo representativo del basquetbol', 1, 5, 'Y19937', 2),
('/assets/Juan-foto.jpeg', NULL, 'Imagen favorita', 'Atardecer y un aro de basquet', 1, 6, 'Y19937', 3),

(NULL, 'https://www.youtube.com/embed/FRn6xXXF-7s?si=yVJP3egE0MB4siuE', 'Mi Video Favorito en YouTube', 'Video del trailer de attack of titan', 1, 7, 'Y25495', 1),
('/assets/Sebastian-Dibujo.jpg', NULL, 'Dibujo favorito', 'Dibujo de una espada en una piedra en llamas', 1, 8, 'Y25495', 2),
('/assets/Sebastian-foto.webp', NULL, 'Imagen favorita', 'Juego de Dark Soul', 1, 9, 'Y25495', 3),

(NULL, 'https://www.youtube.com/embed/NynNdkY2gx0?si=nzQjEF5y6hY6uSOp', 'Mi Video Favorito en YouTube', 'Video de mejores jugadas de futbol', 1, 10, 'Y19099', 1),
('/assets/Elvio-Dibujo.png', NULL, 'Dibujo favorito', 'Dibujo de una cancha de futbol', 1, 11, 'Y19099', 2),
('/assets/Elvio-Foto.jpg', NULL, 'Imagen favorita', 'Cristiano Ronaldo', 1, 12, 'Y19099', 3),

(NULL, 'https://www.youtube.com/embed/Hv5ET7azgEk?si=WxryTH3oIMGTTD-Z', 'Mi Video Favorito en YouTube', 'Video de la vida secreta de la mente', 1, 13, 'UG0085', 1),
('/assets/Luis-Delgado-2.png', NULL, 'Dibujo favorito', 'Dibujo de una computadora', 1, 14, 'UG0085', 2),
('/assets/Luis-Delgado.jpeg', NULL, 'Imagen favorita', 'Foto personal estando en la cima de un cerro', 1, 15, 'UG0085', 3);

-- Home
INSERT INTO home (nombre, titulo, src, alt, descripcion) VALUES
('Devstructors', 'Inicio', '/assets/logo.jpeg', 'Logo del grupo', 'Integrantes: ');

-- Informacion
INSERT INTO informacion (href, nombre) VALUES
('/curso', 'Información del curso'),
('/word_cloud', 'Word Cloud');

-- Usuario
INSERT INTO usuarios (email, contrasenha, super_user, matricula)
VALUES ('Prueba@gmail.com', '$2b$04$vDUIu4rqbLBRziRA/l71ZOU8HWVUNM1b6SyrJyZnsIRoTr/FxFdsa', 1, 'Y25378');
