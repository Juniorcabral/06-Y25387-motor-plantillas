const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const dbPath = path.resolve(__dirname, 'Inge3.db');
const db = new sqlite3.Database(dbPath);

// TODO Para verificar que la conexión con la base de datos sea exitosa
/*db.serialize(() => {
    db.run('SELECT 1', [], (err) => {
        if (err) {
            console.error('Error al conectar con la base de datos:', err);
        } else {
            console.log('Conexión exitosa con la base de datos.');
        }
    });
});*/
// TODO Para encriptar la contraseña
/*const bcrypt = require('bcrypt');
const password = 'Prueba123';
const saltRounds = 1;

bcrypt.hash(password, saltRounds, function(err, hash) {
    console.log(hash);
});*/

module.exports = db;
