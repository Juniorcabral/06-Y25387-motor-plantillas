const authMiddleware = {
    ensureAuthenticated: (req, res, next) => {
        if (req.session.userId) {
            return next();
        }
        res.redirect('/login');
    },
    ensureAdmin: (req, res, next) => {
        if (req.session.superUser) {
            return next();
        }
        res.redirect('/admin');
    }
};

module.exports = authMiddleware;
