const express = require("express");
const router = express.Router();
const multer = require('multer');
const upload = multer({ dest: './public/assets/' });
const fileUpload = upload.single('imagen');
const session = require('express-session');
const flash = require("express-flash");
const authMiddleware = require('../middlewares/auth.middleware');
const AdminIntegrantesController = require('../controllers/admin/integrantes.controller');
const AdminTipoMediaController = require('../controllers/admin/tipoMedia.controller');
const AdminMediaController = require('../controllers/admin/media.controller');
const AdminInicioController = require('../controllers/admin/inicio.controller');

router.use(session({
    secret: 'Inge-3',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
}));
router.use(flash());

router.use((req, res, next) => {
    res.locals.successMessage = req.flash('success');
    res.locals.errorMessage = req.flash('error');
    res.locals.isAuthenticated = !!req.session.userId;
    res.locals.isSuperUser = !!req.session.superUser;
    next();
});

router.use(authMiddleware.ensureAuthenticated);

router.get("/", AdminInicioController.index);

// inicio integrantes
router.get("/integrantes/listar", AdminIntegrantesController.index);

router.get("/integrantes/crear", AdminIntegrantesController.create);

router.post("/integrantes/create", AdminIntegrantesController.store);

router.post("/integrantes/delete/:matricula", AdminIntegrantesController.destroy);

router.get("/integrantes/editar/:matricula", AdminIntegrantesController.edit);

router.post("/integrantes/update/:matricula", AdminIntegrantesController.update);
// fin integrantes

// inicio tipo media
router.get("/tipo_media/listar", AdminTipoMediaController.index);

router.get("/tipo_media/crear", AdminTipoMediaController.create);

router.post("/tipo_media/create", AdminTipoMediaController.store);

router.post("/tipo_media/delete/:tipoMediaID", AdminTipoMediaController.destroy);

router.get("/tipo_media/editar/:tipoMediaID", AdminTipoMediaController.edit);

router.post("/tipo_media/update/:tipoMediaID", AdminTipoMediaController.update);
// fin tipo media

//lista media
router.get("/media/listar", AdminMediaController.index);

router.get("/media/crear", AdminMediaController.create);

router.post("/media/create", fileUpload, AdminMediaController.store);

router.post("/media/delete/:mediaID", AdminMediaController.destroy);

router.get("/media/editar/:mediaID", AdminMediaController.edit);

router.post("/media/update/:mediaID", AdminMediaController.update);
// fin media

module.exports = router;
