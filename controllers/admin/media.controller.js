const db = require("../../models/media.model");
const dbIntegrante = require("../../models/integrantes.model");
const dbTipoMedia = require("../../models/tipoMedia.model");
const fs = require("fs");
const Joi = require('joi');

const mediaSchema = Joi.object({
    integrante: Joi.string().required().messages({
        'string.empty': 'El integrante no debe estar vacío.',
        'any.required': 'El integrante es un campo obligatorio.'
    }),
    tipo_media: Joi.string().required().messages({
        'string.empty': 'El tipo de media no debe estar vacío.',
        'any.required': 'El tipo de media es un campo obligatorio.'
    }),
    orden: Joi.number().integer().required().messages({
        'number.base': 'El orden debe ser un número.',
        'number.integer': 'El orden debe ser un número entero.',
        'any.required': 'El orden es un campo obligatorio.'
    }),
    titulo: Joi.string().required().messages({
        'string.empty': 'El título no debe estar vacío.',
        'any.required': 'El título es un campo obligatorio.'
    }),
    descripcion: Joi.string().required().messages({
        'string.empty': 'La descripción no debe estar vacía.',
        'any.required': 'La descripción es un campo obligatorio.'
    }),
    url: Joi.string().allow('', null),
    tipo: Joi.string().valid('imagen', 'url')
});




const MediaController = {
    index: async function (req, res) {
        const filters = req.query.s || {};
        try {
            const [media, integrantes, tiposMedia] = await Promise.all([
                db.getMedia(filters),
                dbIntegrante.getAllIntegrantes(),
                dbTipoMedia.getTipoMedia()
            ]);
            res.render("admin/media/index", { media, integrantes, tiposMedia });
        } catch (err) {
            res.render("admin/media/index", { error: err });
        }
    },
    create: function (req, res) {
        Promise.all([dbIntegrante.getAllIntegrantes(), dbTipoMedia.getTipoMedia()])
            .then(([integrantes, tipoMedia]) => {
                res.render("admin/media/Crear", { integrantes, tipoMedia });
            })
            .catch((err) => {
                res.render("admin/media/Crear", { error: err });
            });
    },
    store: async function (req, res) {
        const { error, value } = mediaSchema.validate(req.body, { abortEarly: false });
        if (error) {
            // Cargar datos necesarios para el formulario en caso de error
            try {
                const [integrantes, tipoMedia] = await Promise.all([
                    dbIntegrante.getAllIntegrantes(),
                    dbTipoMedia.getTipoMedia()
                ]);
                return res.render("admin/media/Crear", {
                    error: error.details.map(detail => detail.message).join(', '),
                    formData: req.body,
                    integrantes,
                    tipoMedia
                });
            } catch (err) {
                return res.render("admin/media/Crear", {
                    error: "Error al cargar datos necesarios para el formulario: " + err.message
                });
            }
        }

        // Verificar si se subió un archivo
        let destino;
        if (req.file) {
            const tmp_path = req.file.path;
            destino = "./public/assets/" + req.file.originalname;
            try {
                fs.renameSync(tmp_path, destino);
            } catch (err) {
                console.log(err);
                return res.render("admin/media/Crear", { error: "No se pudo guardar la imagen", formData: req.body });
            }
        }

        // Crear media con o sin la imagen
        try {
            await db.createMedia(value, req.file ? '/assets/' + req.file.originalname : null);
            req.flash('success', 'Media creada con éxito');
            res.redirect("/admin/media/listar");
        } catch (err) {
            console.error(err);
            req.flash('error', err.message);
            // Cargar datos nuevamente en caso de error al crear la media
            const [integrantes, tipoMedia] = await Promise.all([
                dbIntegrante.getAllIntegrantes(),
                dbTipoMedia.getTipoMedia()
            ]);
            res.render("admin/media/Crear", { formData: req.body, integrantes, tipoMedia });
        }
    },


    show: function (req, res) {
        //Se implementara en la siguiente issue
    },
    update: async function (req, res) {
        const { error, value } = mediaSchema.validate(req.body, { abortEarly: false });
        const mediaID = req.params.mediaID;

        if (error) {
            try {
                const media = await db.getMediaID(mediaID);
                const [integrantes, tipoMedia] = await Promise.all([
                    dbIntegrante.getAllIntegrantes(),
                    dbTipoMedia.getTipoMedia()
                ]);
                return res.render("admin/media/Editar", {
                    error: error.details.map(detail => detail.message).join(', '),
                    formData: req.body,
                    media,
                    integrantes,
                    tipoMedia
                });
            } catch (err) {
                console.error(err);
                req.flash('error', '¡Error al cargar los datos de la media!: ' + err);
                return res.redirect("/admin/media/listar");
            }
        }

        try {
            await db.updateMedia(mediaID, value);
            req.flash('success', '¡Media actualizada con éxito!');
            res.redirect("/admin/media/listar");
        } catch (err) {
            console.error(err);
            req.flash('error', '¡Error al actualizar la media!: ' + err);
            res.redirect("/admin/media/listar");
        }
    },

    edit: function (req, res) {
        db.getMediaID(req.params.mediaID)
            .then((media) => {
                res.render("admin/media/Editar", { media });
            })
            .catch((err) => {
                res.render("admin/media/Editar", { error: err });
            });
    },
    destroy: function (req, res) {
        db.deleteMedia(req.params.mediaID)
            .then(() => {
                req.flash('success', '¡Media eliminada con éxito!');
                res.redirect("/admin/media/listar");
            })
            .catch((err) => {
                console.error(err);
                req.flash('error', '¡Error al eliminar la media!: ' + err);
                res.redirect("/admin/media/listar");
            });
    }
}

module.exports = MediaController;
