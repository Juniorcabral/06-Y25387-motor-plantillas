/*
index - listado
create - formulario de creación
store - método de guardar en la base de datos
show - formulario de ver un registor
update - método de editar un registro
edit - formulario de edición
destroy - operación de eliminar un registro
*/

const db = require("../../models/integrantes.model");
const Joi = require('joi');

const integranteSchema = Joi.object({
    nombre: Joi.string().required().messages({
        'string.empty': 'El nombre no debe estar vacío.',
        'any.required': 'El nombre es un campo obligatorio.'
    }),
    apellido: Joi.string().required().messages({
        'string.empty': 'El apellido no debe estar vacío.',
        'any.required': 'El apellido es un campo obligatorio.'
    }),
    matricula: Joi.string().required().messages({
        'string.empty': 'La matrícula no debe estar vacía.',
        'any.required': 'La matrícula es un campo obligatorio.'
    }),
    descripcion: Joi.string().required().messages({
        'string.empty': 'La descripción no debe estar vacía.',
        'any.required': 'La descripción es un campo obligatorio.'
    }),
    orden: Joi.number().integer().required().messages({
        'number.base': 'El orden debe ser un número.',
        'number.integer': 'El orden debe ser un número entero.',
        'any.required': 'El orden es un campo obligatorio.'
    })
});


const IntegrantesController = {
    index: function (req, res) {
        const filters = req.query.s || {};
        db.getAllIntegrantes(filters).then((integrantes) => {
            res.render("admin/integrantes", { integrantes });
        }).catch((err) => {
            res.render("admin/integrantes", { error: err });
        });
    },
    create: function (req, res) {
        res.render("admin/integrantes/Crear");
    },
    store: async (req, res) => {
        const { error, value } = integranteSchema.validate(req.body, { abortEarly: false });
        if (error) {
            return res.render("admin/integrantes/Crear", {
                error: error.details.map(detail => detail.message).join(', '),
                formData: req.body
            });
        }
        try {
            await db.createIntegrante(value);
            req.flash('success', '¡Integrante creado con éxito!');
            res.redirect("/admin/integrantes/listar?success=Integrante creado con éxito");
        } catch (err) {
            res.render('admin/integrantes/Crear', {error: err.message, formData: req.body});
        }
    },
    show: function (req, res) {
        //Se implementara en la siguiente issue
    },
    update: async (req, res) => {
        const { error, value } = integranteSchema.validate(req.body, { abortEarly: false });
        if (error) {
            try {
                const integrante = await db.getIntegranteByMatricula(req.params.matricula);
                return res.render("admin/integrantes/Editar", {
                    error: error.details.map(detail => detail.message).join(', '),
                    formData: req.body,
                    integrante
                });
            } catch (err) {
                console.error(err);
                req.flash('error', '¡Error al cargar los datos del integrante!: ' + err);
                return res.redirect("/admin/integrantes/listar");
            }
        }

        try {
            await db.editIntegrante(req.params.matricula, value);
            req.flash('success', '¡Integrante actualizado con éxito!');
            res.redirect("/admin/integrantes/listar");
        } catch (err) {
            console.error(err);
            req.flash('error', '¡Error al actualizar el integrante!: ' + err);
            res.redirect("/admin/integrantes/listar");
        }
    },

    edit: function (req, res) {
        db.getIntegranteByMatricula(req.params.matricula)
            .then((integrante) => {
                res.render("admin/integrantes/Editar", { integrante });
            })
            .catch((err) => {
                res.render("admin/integrantes/Editar", { error: err });
            });
    },
    destroy: function (req, res) {
        db.deleteIntegrante(req.params.matricula)
            .then(() => {
                req.flash('success', '¡Integrante eliminado con éxito!');
                res.redirect("/admin/integrantes/listar");
            })
            .catch((err) => {
                console.error(err);
                req.flash('error', '¡Error al eliminar el integrante!: '+ err);
                res.redirect("/admin/integrantes/listar");
            });
    }
}

module.exports = IntegrantesController;
