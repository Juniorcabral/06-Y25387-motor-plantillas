const db = require("../../models/tipoMedia.model");
const Joi = require('joi');

const tipoMediaSchema = Joi.object({
    nombre: Joi.string().required().messages({
        'string.empty': 'El nombre no debe estar vacío.',
        'any.required': 'El nombre es un campo obligatorio.'
    }),
    orden: Joi.number().integer().required().messages({
        'number.base': 'El orden debe ser un número.',
        'number.integer': 'El orden debe ser un número entero.',
        'any.required': 'El orden es un campo obligatorio.'
    })
});

const TipoMediaController = {
    index: function (req, res) {
        const filters = req.query.s || {};
        db.getTipoMedia(filters).then((tipoMedia) => {
            res.render("admin/tipo_media", { tipoMedia });
        }).catch((err) => {
            res.render("admin/tipo_media", { error: err });
        });
    },
    create: function (req, res) {
        res.render("admin/tipo_media/Crear");
    },
    store: async function (req, res) {
        const { error, value } = tipoMediaSchema.validate(req.body, { abortEarly: false });
        if (error) {
            return res.render("admin/tipo_media/Crear", {
                error: error.details.map(detail => detail.message).join(', '),
                formData: req.body
            });
        }
        try {
            await db.createTipoMedia(value);
            req.flash('success', 'Tipo de media creado con éxito');
            res.redirect("/admin/tipo_media/listar");
        } catch (err) {
            req.flash('error', err.message);
            res.render("admin/tipo_media/Crear", { formData: req.body });
        }
    },
    show: function (req, res) {
        //Se implementara en la siguiente issue
    },
    update: async function (req, res) {
        const { error, value } = tipoMediaSchema.validate(req.body, { abortEarly: false });
        if (error) {
            try {
                const tipoMedia = await db.getTipoMediaByID(req.params.tipoMediaID);
                return res.render("admin/tipo_media/Editar", {
                    error: error.details.map(detail => detail.message).join(', '),
                    formData: req.body,
                    tipoMedia
                });
            } catch (err) {
                console.error(err);
                req.flash('error', '¡Error al cargar los datos del tipo de media!: ' + err);
                return res.redirect("/admin/tipo_media/listar");
            }
        }

        try {
            await db.editTipoMedia(value, req.params.tipoMediaID);
            req.flash('success', '¡Tipo de media actualizado con éxito!');
            res.redirect("/admin/tipo_media/listar");
        } catch (err) {
            console.error(err);
            req.flash('error', '¡Error al actualizar el tipo de media!: ' + err);
            res.redirect("/admin/tipo_media/listar");
        }
    },

    edit: function (req, res) {
        db.getTipoMediaByID(req.params.tipoMediaID)
            .then((tipoMedia) => {
                res.render("admin/tipo_media/Editar", { tipoMedia });
            })
            .catch((err) => {
                res.render("admin/tipo_media/Listar", { error: err });
            });
    },
    destroy: function (req, res) {
        db.deleteTipoMedia(req.params.tipoMediaID)
            .then(() => {
                req.flash('success', '¡Tipo de media eliminado con éxito!');
                res.redirect("/admin/tipo_media/listar");
            })
            .catch((err) => {
                console.error(err);
                req.flash('error', '¡Error al eliminar el tipo de media!: '+ err);
                res.redirect("/admin/tipo_media/listar");
            });
    }
}

module.exports = TipoMediaController;