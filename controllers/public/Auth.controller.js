const bcrypt = require('bcrypt');
const { getUserByEmail } = require('../../models/usuarios.model');

const AuthController = {
    loginForm: (req, res) => {
        res.render('login');
    },
    login: async (req, res) => {
        const { email, password } = req.body;
        try {
            const user = await getUserByEmail(email);
            if (user && await bcrypt.compare(password, user.contrasenha)) {
                req.session.userId = user.id;
                //req.session.superUser = user.super_user;
                res.redirect('/');
            } else {
                res.render('login', { error: 'Email o contraseña incorrectos', email });
            }
        } catch (err) {
            res.render('login', { error: 'Error al iniciar sesión', email });
        }
    },
    logout: (req, res) => {
        req.session.destroy((err) => {
            if (err) {
                return res.redirect('/');
            }
            res.clearCookie('sid');
            res.redirect('/');
        });
    }
};

module.exports = AuthController;
