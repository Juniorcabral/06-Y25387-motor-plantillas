const { getMedia } = require('../../models/media.model');
const { getHome, getInformacion } = require('../../models/home.model');
const { getTipoMedia } = require('../../models/tipoMedia.model');
const { getIntegranteByMatricula, getAllIntegrantes } = require('../../models/integrantes.model');

require('dotenv').config();

const PublicController = {
    indexNavbar: async (req, res, next) => {
        try {
            res.locals.integrantes = await getAllIntegrantes();
            res.locals.isAuthenticated = !!req.session.userId;
            //res.locals.isSuperUser = !!req.session.superUser;
            next();
        } catch (err) {
            next(err);
        }
    },
    indexFooter: async (req, res, next) => {
        res.locals.gitlabUrl = process.env.GITLAB_URL;
        res.locals.nombre = process.env.NOMBRE;
        res.locals.matricula = process.env.MATRICULA;
        res.locals.materia = process.env.MATERIA;
        next();
    },
    index: async (req, res, next) => {
        try {
            const home = await getHome();
            const informacion = await getInformacion();
            res.render('index', {
                home: home[0],
                curso: informacion[0],
                wordCloud: informacion[1],
                isHome: true
            });
        } catch (err) {
            next(err);
        }
    },
    indexCurso: (req, res) => {
        res.render('curso');
    },
    indexWordCloud: (req, res) => {
        res.render('word_cloud');
    },
    indexIntegrante: async (req, res, next) => {
        try {
            const matricula = req.params.matricula;
            const integrante = await getIntegranteByMatricula(matricula);
            if (integrante) {
                res.locals.integrante = [integrante];
                next();
            } else {
                res.status(404).render('404_page');
            }
        } catch (err) {
            next(err);
        }
    },
    indexIntegranteByMatricula: async (req, res, next) => {
        try {
            const matricula = req.params.matricula;
            const mediaFiltrada = await getMedia({ matricula });
            const tipoMedia = await getTipoMedia();
            res.render('Integrante', {
                tipoMedia: tipoMedia,
                media: mediaFiltrada,
                integrante: res.locals.integrante
            });
        } catch (err) {
            next(err);
        }
    }
}

module.exports = PublicController;